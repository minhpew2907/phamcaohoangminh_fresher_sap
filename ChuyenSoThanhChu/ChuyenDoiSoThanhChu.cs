﻿public class ChuyenDoiSoThanhChu
{
    public string ChuyenSoThanhChu(int number)
    {
        var arrChar = number.ToString().ToCharArray();
        int lengthOfarrChar = arrChar.Length;
        switch (lengthOfarrChar)
        {
            case 1:
                return SoCoMotDonVi(number);
            case 2:
                return SoCoHaiDonVi(number);
            case 3:
                return SoCoBaDonVi(number);
            default:
                throw new Exception("not exists");
        }
    }

    public string SoCoHaiDonVi(int number)
    {
        var mangKyTu = number.ToString().ToCharArray();

        switch (mangKyTu[1])
        {
            case '0':
                return SoCoMotDonVi(int.Parse(mangKyTu[0].ToString())) + "mươi";
            case '1':
                return SoCoMotDonVi(int.Parse(mangKyTu[0].ToString())) + "mươi" + "mốt";
            case '5':
                return SoCoMotDonVi(int.Parse(mangKyTu[0].ToString())) + "mươi" + "lăm";
            case '9':
                return "mười" + SoCoMotDonVi(int.Parse(mangKyTu[1].ToString()));
            default:
                return SoCoMotDonVi(int.Parse(mangKyTu[0].ToString())) + "mươi" + SoCoMotDonVi(int.Parse(mangKyTu[1].ToString()));
        }
    }

    public string SoCoBaDonVi(int number)
    {
        var mangKyTu = number.ToString().ToCharArray();

        switch (mangKyTu[2])
        {
            case '0':
                return SoCoMotDonVi(int.Parse(mangKyTu[0].ToString())) + "trăm";
            case '1':
                return SoCoMotDonVi(int.Parse(mangKyTu[0].ToString())) + "trăm" + SoCoMotDonVi(int.Parse(mangKyTu[1].ToString())) + "mươi" + "mốt";
            case '5':
                return SoCoMotDonVi(int.Parse(mangKyTu[0].ToString())) + "trăm" + SoCoMotDonVi(int.Parse(mangKyTu[0].ToString())) + "mươi" + "lăm";
            case '2':
                return SoCoMotDonVi(int.Parse(mangKyTu[0].ToString())) + "trăm" + "linh" + SoCoMotDonVi(int.Parse(mangKyTu[0].ToString()));
            default:
                return SoCoMotDonVi(int.Parse(mangKyTu[0].ToString())) + "trăm" + SoCoMotDonVi(int.Parse(mangKyTu[0].ToString())) + "mươi" + SoCoMotDonVi(int.Parse(mangKyTu[1].ToString()));
        }
    }

    public string SoCoMotDonVi(int number)
    {
        switch (number)
        {
            case 0:
                return "không";
            case 1:
                return "một";
            case 2:
                return "hai";
            case 3:
                return "ba";
            case 4:
                return "bốn";
            case 5:
                return "năm";
            case 6:
                return "sáu";
            case 7:
                return "bảy";
            case 8:
                return "tám";
            case 9:
                return "chín";
            default:
                return " ";
        }
    }
}


