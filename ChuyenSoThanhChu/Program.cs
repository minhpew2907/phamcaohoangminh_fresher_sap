﻿
using System.Linq.Expressions;

Console.OutputEncoding = System.Text.Encoding.UTF8;
ChuyenDoiSoThanhChu chuyenNumber = new ChuyenDoiSoThanhChu();
try
{
    Console.WriteLine("Kiểm tra thử chuyển số thành chữ: ");
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(0));
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(10));
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(19));
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(231));
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(101));
    Console.WriteLine("\nKiểm thử các số khác: ");
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(29));
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(7));
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(21));
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(9));
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(8));
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(6));
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(7));
    Console.WriteLine(chuyenNumber.ChuyenSoThanhChu(291));
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}


