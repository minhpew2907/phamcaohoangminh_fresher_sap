USE MASTER
GO
DROP DATABASE IF EXISTS CustomerAndOrders
GO
CREATE DATABASE CustomerAndOrders
GO 
USE CustomerAndOrders;

CREATE TABLE Customers(
	Customer_number INT PRIMARY KEY,
	Customer_name VARCHAR(255)
);

CREATE TABLE Orders(
	Order_number INT PRIMARY KEY,
	Customer_number INT FOREIGN KEY REFERENCES Customers (Customer_number),
	Order_quantity INT,	
);

INSERT INTO Customers(Customer_number, Customer_name)
VALUES (1, 'Nguyen Van A'),
	   (2, 'Company B'),
	   (3, 'Le Van C'),
	   (4, 'Company D'),
	   (5, 'Company E'),
	   (6, 'Tran Thi P');

INSERT INTO Orders(Order_number, Customer_number, Order_quantity)
VALUES (1, 1, 100),
       (2, 2, 1000),
       (3, 4, 5000),
       (4, 2, 2500),
       (5, 4, 6000),
       (6, 3, 800),
       (7, 5, 10000),
       (8, 6, 921);

--Create an SQL query that shows the TOP 3 Customers who bought the most goods in total!
SELECT TOP 3 c.Customer_number, c.Customer_name, SUM(od.Order_quantity) AS TotalOfBoughtGoods
FROM Customers c
JOIN Orders od ON c.Customer_number = od.Customer_number
GROUP BY c.Customer_number, c.Customer_name
ORDER BY TotalOfBoughtGoods DESC
